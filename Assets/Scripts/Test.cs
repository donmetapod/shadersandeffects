﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
	private float value = 0;
	
	// Update is called once per frame
	void Update ()
	{
		GetComponent<Renderer>().material.SetFloat("_MySliderValue", value += Time.deltaTime);
	}
}
